{
  "session": {
    "new": false,
    "sessionId": "session1234",
    "attributes": {
      "option":  "ListSides",
      "sides" : "Cole slaw and pickles"
    },
    "user": {
      "userId": null
    },
    "application": {
      "applicationId": "amzn1.echo-sdk-ams.app.[unique-value-here]"
    }
  },
  "version": "1.0",
  "request": {
    "intent": {
      "slots": {
      },
      "name": "YesResponse"
    },
    "type": "IntentRequest",
    "requestId": "request5678"
  }
}