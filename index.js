"use strict";

var aws = require('aws-sdk');

var schedule = "";

var DATE_STATUS_INVALID = "Invalid Date";
var DATE_STATUS_NOT_SPECIFIED = "Not specified";
var DATE_STATUS_OK = "OK";
var SCHOOL_NOT_SPECIFIED = "Not specified";
var SCHOOL_OK = "OK";
var SCHOOL_UNKNOWN = "Unknown";

var NEXT_DAY = "Next Day";

var millisPerDay = 1000 * 60 * 60 * 24;

var whichSchoolPrompt = "Do you want the elementary or middle school menu";
var nextDayPrompt = "Do you want to hear the menu for the next day?";

/**
 * This sample demonstrates a simple skill built with the Amazon Alexa Skills Kit.
 * For additional samples, visit the Alexa Skills Kit developer documentation at
 * https://developer.amazon.com/appsandservices/solutions/alexa/alexa-skills-kit/getting-started-guide
 */

function getSchoolFromSlot (schoolSlot) {
  var value = schoolSlot.value;
  console.log ("In getSchoolFromSlot, school value is " + value);
  if (value && (value === "creekside" || value === "middle" || value === "middle school")) {
    return "middle";
  }
  
  if (value === "new gronigen" || value === "elementary") {
    return "elementary";
  }
  
  if (value === "both") {
    return "both";
  }
  
  return SCHOOL_UNKNOWN;
}

// Route the incoming request based on type (LaunchRequest, IntentRequest,
// etc.) The JSON body of the request is provided in the event parameter.
exports.handler = function (event, context) {
  try {
    console.log("event.session.application.applicationId=" + event.session.application.applicationId);

    if (event.session.new) {
          onSessionStarted({requestId: event.request.requestId}, event.session);
        }

    if (event.request.type === "LaunchRequest") {
      onLaunch(event.request,
               event.session,
               function callback(sessionAttributes, speechletResponse) {
                 context.succeed(buildResponse(sessionAttributes, speechletResponse));
               }
      );
    } else if (event.request.type === "IntentRequest") {
      onIntent(event.request,
               event.session,
               function callback(sessionAttributes, speechletResponse) {
        context.succeed(buildResponse(sessionAttributes, speechletResponse));
      }
              );
    } else if (event.request.type === "SessionEndedRequest") {
      onSessionEnded(event.request, event.session);
      context.succeed();
    }

    /**
         * Uncomment this if statement and populate with your skill's application ID to
         * prevent someone else from configuring a skill that sends requests to this function.
         */
    /*
        if (event.session.application.applicationId !== "amzn1.echo-sdk-ams.app.[unique-value-here]") {
             context.fail("Invalid Application ID");
         }
        */
  } catch (e) {
    context.fail("Exception: " + e);
  }
};

/**
 * Called when the session starts.
 */
function onSessionStarted(sessionStartedRequest, session) {
  console.log("onSessionStarted requestId=" + sessionStartedRequest.requestId
              + ", sessionId=" + session.sessionId);
}

/**
 * Called when the user launches the skill without specifying what they want.
 */
function onLaunch(launchRequest, session, callback) {
  console.log("onLaunch requestId=" + launchRequest.requestId
              + ", sessionId=" + session.sessionId);
  // Dispatch to your skill's launch.
  getWelcomeResponse(callback);
}

/**
 * Called when the user specifies an intent for this skill.
 */
function onIntent(intentRequest, session, callback) {
  console.log("onIntent requestId=" + intentRequest.requestId
              + ", sessionId=" + session.sessionId);

  var intent = intentRequest.intent,
      intentName = intentRequest.intent.name;

  if ("GetMenu" === intentName) {
   
    if (session.attributes && session.attributes.schedule) {
      console.log ("Found schedule in session, so not requesting from S3");
      schedule = session.attributes.schedule;
      getMenu(session, intent, callback);
    }
    else {
    
        var s3 = new aws.S3();

        var object = s3.getObject({Bucket: 'mcfall-zps-lunch-menu', Key: 'menu.json'}, function (err, data) {
          if (err) {
            console.log("Error calling getObject");
          } else {
            console.log("Returned from getObject; calling getMenu");
            schedule = JSON.parse(data.Body);
            getMenu(session, intent, callback);
          }
        });
    }
  }
  else if ("Quit" === intentName) {
      sayGoodbye(session, callback);
  }
  else if ("ListSides" === session.attributes.option && "YesResponse" === intentName) {
    console.log("Sides requested; listing sides and prompting for the next day's menu");
    session.attributes.option = NEXT_DAY;
    ListSides(session, callback);
  }
  else if ("ListSides" === session.attributes.option && "NoResponse" === intentName) {
    console.log("Sides not requested; prompting for the next day's menu");
    session.attributes.option = NEXT_DAY;
    callback(session.attributes, buildSpeechletResponse("Lunch menu", nextDayPrompt, nextDayPrompt, false));
  }
  else if (NEXT_DAY === session.attributes.option && "YesResponse" === intentName) {
    session.attributes.date = session.attributes.nextDate;
    console.log("Next day requested; set date in session to be " + session.attributes.date);
    getMenu(session, intent, callback);
  }
  else if (NEXT_DAY === session.attributes.option && "NoResponse" === intentName) {
    sayGoodbye(session, callback);
  }    
  else {
    throw "Invalid intent";
  }
}

function sayGoodbye (session, callback) {
    var farewell = [
      "aloha", "adios", "sayanora", "farewell", "au revoir"  
    ];
    var index = Math.floor(Math.random() * farewell.length);
    var goodbyeString = farewell[index];
    
    console.log("Next day's menu not requested; exiting");
    callback(session.attributes, buildSpeechletResponse("Lunch menu", "OK, " + goodbyeString + "!", "OK, " + goodbyeString + "!", true));
}

function ListSides(session, callback) {
  var sides = session.attributes.sides;
  var date = new Date(session.attributes.date);
  
  var title = "Sides for " + formatDateForSpeech(date);
  callback(session.attributes,  buildSpeechletResponse(title, "The sides are: " + sides + ".  " + nextDayPrompt, "", false));
}

function getDateFromSlotOrSession (intent, session) {
  console.log("in getDateFromSlotOrSession");
  
  var result = {
    status: DATE_STATUS_NOT_SPECIFIED,
    value: new Date(0)
  };
  
  var attributes = session.attributes;
  if (attributes && attributes.date) {
    console.log("Found date in session");
    result.value = new Date(attributes.date);
    result.status = DATE_STATUS_OK;
    return result;
  }
  else {
    console.log("Could not find date in session");
  }
  
  var dateSlot = intent.slots.day;
  if (dateSlot) {
    console.log("Date slot is defined");
    var date = new Date(dateSlot.value);
    if (date.toString() === "Invalid Date") {
      console.log("Date is invalid");
      result.status = DATE_STATUS_INVALID;
    }
    else {
      console.log("Found valid date " + formatDateForSpeech(date));
      result.value = date;
      result.status = DATE_STATUS_OK;
    }
  }
  
  return result;
}

function getSchoolFromSlotOrSession (intent, session) {
  var result = {
    status: SCHOOL_NOT_SPECIFIED,
    value: ""
  };
  
  var attributes = session.attributes;
  if (attributes && attributes.school) {
    result.value = attributes.school;
    result.status = SCHOOL_OK;
    console.log("Found school in session: " + result.value);
    return result;
  }
  
  var schoolSlot = intent.slots.school;
  if (schoolSlot) {
    console.log("School slot is defined");
    var school = getSchoolFromSlot(schoolSlot);
    if (school != SCHOOL_UNKNOWN) {
      result.status = SCHOOL_OK;
      result.value = school;
      console.log("School set to " + school + " from slot");
    }
    else {
      console.log("School is unknown from slot");
      result.status = SCHOOL_UNKNOWN;
    }  
  }
  else {
    console.log ("School slot is undefined");
  }
  return result;
}

function getMenu(session, intent, callback) {
  console.log("In getMenu");
  
  var days = [
    "sunday", "monday", "tuesday", "wednesday", "thursday", "friday",
    "saturday" 
  ];
  
  var sessionAttributes = {};
  sessionAttributes.schedule = schedule;
  
  var dateResult = getDateFromSlotOrSession(intent, session);
  var schoolResult = getSchoolFromSlotOrSession(intent, session);
  
  if (dateResult.status == DATE_STATUS_OK) {
    console.log("In getMenu, date is " + date);
    var date = dateResult.value;
    sessionAttributes.date = date.getTime();
  }
  
  if (schoolResult.status == SCHOOL_OK) {
      var school = schoolResult.value;  
      sessionAttributes.school = school;
  }
  
  if (dateResult.status === DATE_STATUS_NOT_SPECIFIED) {
    var repromptText = 
        "Which date do you want the menu for?" +
        "You can say things like tomorrow, next Wednesday, or a specific date like " +
        "September First";
    
    callback({}, buildSpeechletResponse("Lunch Menu", repromptText, repromptText, false));
    return;
  }
  
  if (dateResult.status == DATE_STATUS_INVALID) {
    var repromptText = 
        "I didn't understand the date you asked for.  " +
        "You can say things like tomorrow, next Wednesday, or a specific date like " +
        "September First, October Second, or November twenty fifth.  What day do you want?";
    
    callback(sessionAttributes, buildSpeechletResponse("Lunch Menu", repromptText, repromptText, false));
    return;
  }
  
  if (schoolResult.status == SCHOOL_NOT_SPECIFIED) {
    callback(sessionAttributes, buildSpeechletResponse("Lunch menu", whichSchoolPrompt, whichSchoolPrompt, false));
    return;
  }
  
  if (schoolResult.status === SCHOOL_UNKNOWN) {
    callback(sessionAttributes, 
             buildSpeechletResponse("Lunch menu", whichSchoolPrompt, "Sorry, I didn't understand the school you said. " + whichSchoolPrompt, false)
    );
    return;
  }
  
  var week = getWeekForDate(schedule, date);
  console.log ("Week is " + week);
  
  if (week == -1) {
    var dateRange = getDateRange(schedule);
    var output = "I heard " + formatDateForSpeech(date) + ".  I currently have the lunch menu for " + formatDateForSpeech(dateRange.min) + " through " + formatDateForSpeech(dateRange.max) + ".  Choose another date";
    delete sessionAttributes.date;
    callback(sessionAttributes,buildSpeechletResponse("Lunch menu", output, output, false));
    return;
  }
  
  var dayOfWeek = date.getDay();
  if (dayOfWeek == 0 || dayOfWeek == 6) {
    var output = formatDateForSpeech(date) + " is a " + days[dayOfWeek] + ".  What date do you want?";
    delete sessionAttributes.date;
    callback(sessionAttributes, buildSpeechletResponse("Lunch menu", output, output, false));
    return;
  }
  
  var day = days[dayOfWeek];
  
  console.log("getDay returns " + date.getDay());
  respondWithMenu(callback, school, date, week, day, sessionAttributes);
}

function respondWithMenu (callback, school, date, week, day, sessionAttributes) {
  var choices = [
    "A: " + schedule[school][week][day]["A"],
    "B: " + schedule[school][week][day]["B"],
    "C: " + schedule[school][week][day]["C"]
  ];
  
  if (schedule[school][week][day]["D"]) {
     choices.push("D. " + schedule[school][week][day]["D"]);
  }
  
  var speechOutput = "Here's the " + school + " lunch menu for " + formatDateForSpeech(date) +":";
  
  for (var index = 0; index < choices.length; index++) {
    speechOutput += ", " + choices[index];
  }
  
  speechOutput += ".  Do you want to hear the sides?";
  
  sessionAttributes.option = "ListSides";
  sessionAttributes.sides = schedule[school][week][day]["sides"];
  sessionAttributes.nextDate = getNextDate(date, day);
  
  console.log ("Set next date in session to " + sessionAttributes.nextDate);
  console.log ("Set session attribute 'option' to " + sessionAttributes.option);
  console.log ("Set session attribute 'sides' to " + sessionAttributes.sides);
  
  var title = "Lunch Menu";
  var repromptText = "I couldn't understand the day you want the menu for";
  
  callback(sessionAttributes, buildSpeechletResponse(title, speechOutput, repromptText, false));
}

function getNextDate (date, day) {
  var nextDate;
  
  //  Check for Friday
  if (day == 5) {
    nextDate = date.getTime() + 3 * millisPerDay;
  }
  else {
    nextDate = date.getTime() + millisPerDay;
  }
  
  return nextDate;
}

function getWeekForDate (schedule, date) {

  var possibleWeeks = ["1", "2", "3"];

  for (var j = 0; j < possibleWeeks.length; j++) {
    var dates = schedule.schedule[possibleWeeks[j]];
    for (var i = 0; i < dates.length; i++) {
      var startDate = new Date(dates[i]);  
      var endDate = new Date(startDate.getTime() + 5 * millisPerDay);
      if (date >= startDate && date <= endDate) {
        return possibleWeeks[j];
      }
    }
  }
  
  return -1;
}

function getDateRange (schedule) {
  var minDate = new Date(schedule.schedule["1"][0]);
  var maxDate = minDate;
  
  var possibleWeeks = ["1", "2", "3"];
  for (var j = 0; j < possibleWeeks.length; j++) {
    var dates = schedule.schedule[possibleWeeks[j]];
    for (var i = 0; i < dates.length; i++) {
      var checkDate = new Date(dates[i]);
      if (checkDate < minDate) {
        minDate = checkDate;
      }
      
      if (checkDate > maxDate) {
        maxDate = checkDate;
      }
    }
  }
  
  return {"min": minDate, "max": maxDate};
}

function formatDateForSpeech (date) {
    var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    return monthNames[monthIndex] + " " + day; 
}

/**
 * Called when the user ends the session.
 * Is not called when the skill returns shouldEndSession=true.
 */
function onSessionEnded(sessionEndedRequest, session) {
  console.log("onSessionEnded requestId=" + sessionEndedRequest.requestId
              + ", sessionId=" + session.sessionId);
  // Add cleanup logic here
}

// --------------- Helpers that build all of the responses -----------------------
function getWelcomeResponse(callback) {
    // If we wanted to initialize the session to have some attributes we could add those here.
    var sessionAttributes = {};
    var repromptText = "Sorry, I didn't hear you.  Which date do you want the menu for";
    var speechOutput = "Welcome to lunch menu.  Which date do you want the menu for?";
    
    // If the user either does not reply to the welcome message or says something that is not
    // understood, they will be prompted again with this text.
    callback({},buildSpeechletResponse("Lunch Menu", speechOutput, repromptText, false));
}

function buildSpeechletResponse(title, output, repromptText, shouldEndSession) {
  return {
    outputSpeech: {
      type: "PlainText",
      text: output
    },
    card: {
      type: "Simple",
      title: "SessionSpeechlet - " + title,
      content: "SessionSpeechlet - " + output
    },
    reprompt: {
      outputSpeech: {
        type: "PlainText",
        text: repromptText
      }
    },
    shouldEndSession: shouldEndSession
  }
}

function buildResponse(sessionAttributes, speechletResponse) {
  return {
    version: "1.0",
    sessionAttributes: sessionAttributes,
    response: speechletResponse
  }
}