GetMenu for {tomorrow's|day} menu
GetMenu what's on the {elementary|school} menu for {tomorrow|day}
GetMenu what's on the {elementary|school} menu for {September ninth|day}
GetMenu what's on the {elementary|school} menu for {September eight|day}
GetMenu what's for lunch {tomorrow|day} at {Creekside|school}
GetMenu what's for lunch {September ninth|day} at {Creekside|school}
GetMenu what's for lunch {October seventh|day} at {New Gronigen|school}
GetMenu what's for lunch {November seventh|day}
GetMenu what's for lunch {November eleventh|day}
GetMenu for the {elementary|school} menu for {September ninth|day}
GetMenu {September ninth|day}
GetMenu {October tenth|day}
GetMenu {November third|day}
GetMenu {Creekside|school}
GetMenu {Middle|school}
GetMenu {middle school|school}
GetMenu {Both|school}
GetMenu {Elementary|school}
GetMenu {tomorrow|day}
YesResponse yes
NoResponse no
Quit quit
Quit stop
Quit bye
Quit done
Quit never mind